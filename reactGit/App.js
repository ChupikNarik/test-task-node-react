/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {View, Text, ScrollView, Linking, StyleSheet} from 'react-native';
import axios from 'react-native-axios';
import { Card, Button } from 'react-native-elements';

export default class App extends Component{
  constructor(props){
    super(props);
    this.state={
        onLoad: false,
        users: []
    }
  }

  componentDidMount(){
    axios.get("http://localhost:3012/").then(
      (response) => {
          this.setState({users: []});
          this.setState({users: response.data});
          this.setState({onLoad: true});
      })
  }

  renderUsers() {
    return this.state.users.map((item, index) => <View>
      <Card title={item.name}>
        <Text style={{marginBottom: 10}}>Login: {item.login}</Text>
        <Text style={{marginBottom: 10}}>Location: {item.location}</Text>
        <Text style={{marginBottom: 10}}>Email: {item.email}</Text>
        <Text style={{marginBottom: 10}}>{item.bio}</Text>
        <Button
          backgroundColor='#03A9F4'
          buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}
          title='GitHub profile'
          onPress={ ()=> Linking.openURL(item.url) }/>
      </Card>
    </View>);
  }

  render() {
    if (this.state.onLoad != false) {
      return (
        <ScrollView style={styles.scroll}>
          {this.renderUsers()}
        </ScrollView>
      );
    }else{
      return(
        <ScrollView style={styles.scroll}>
          <Text style={styles.loading}>Загрузка...</Text>
        </ScrollView>
      )
    }
  }
}

const styles = StyleSheet.create({
  scroll: {
    paddingTop: 60
  },
  loading:{
    textAlign: 'center',
    fontSize: 28
  }
});
