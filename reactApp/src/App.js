import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
import EmailIcon from '@material-ui/icons/Email';
import PlaceIcon from '@material-ui/icons/Place';

import axios from 'axios';

const styles = {
  card: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
    display: 'flex',
    alignItens: 'center'
  },
  icon: {
    marginRight: 5
  }
};

// function SimpleCard(props) {
class SimpleCard extends React.Component {

  state = {
    persons: []
  }

  componentDidMount() {
    axios.get(`http://localhost:3012/`)
      .then(res => {
        const persons = res.data;
        this.setState({ persons });
        console.log(this.state.persons);
      })
  }

  render() {
    const { classes } = this.props;
    const bull = <span className={classes.bullet}>•</span>;

    return (
      <div>
        { this.state.persons.map(person =>
          <Card className={classes.card}>
            <CardContent>
              <Typography className={classes.title} color="textSecondary" gutterBottom>
                Login: 
                  <b> {person.login}</b>
              </Typography>
              <Typography variant="h5" component="h2">
                {person.name}
              </Typography>
              <Typography className={classes.pos} color="textSecondary">
                <PlaceIcon className={classes.icon} />
                {person.location}
              </Typography>
              <Typography className={classes.pos} color="textSecondary">
                <EmailIcon className={classes.icon} />
                {person.email}
              </Typography>
              <Typography component="p">
                {person.bio}
              </Typography>
            </CardContent>
            <CardActions>
              <a href={person.url} target="_blank">
                <Button size="small">GitHub profile</Button>
              </a>
            </CardActions>
          </Card>)}
       </div>
    );
  }
}

SimpleCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SimpleCard);