var express = require('express');
var bodyParser = require('body-parser');
var db = require('./db');
var usersController = require('./controllers/users');

var result;
var express_graphql = require('express-graphql');
var { buildSchema } = require('graphql');

const fetch = require('node-fetch');
var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

// db.connect('mongodb://localhost:27017/github-test', function(err){
db.connect('mongodb://test-user:test-password@easy-web.solutions:27017/test-git', function(err){
	if (err) {
		return console.log(err);
	}
	app.listen(3012, function () {
		console.log('API app started');
	})
});

const accessToken = 'c3da25813973a0deebcc50f0f56f56069a804c48';
const query = `
	query {
		search(query: "location:Kyiv sort:followers", type: USER, first: 10) {
			userCount
			edges {
				node {
					... on User {
						login
						name
						location
						email
						bio
						url
					}
				}
			}
		}
	}`;

function getUsers(){
	fetch('https://api.github.com/graphql', {
		method: 'POST',
		body: JSON.stringify({query}),
		headers: {
			'Authorization': `Bearer ${accessToken}`,
		},
	}).then(
		res => res.text()
	).then(
		body => {
			result = body;
			usersController.clear();
			console.log('Database updated');
			for (var i = 0; i <= JSON.parse(result).data.search.edges.length; i++) {
				var userArray = JSON.parse(result).data.search.edges[i].node;
				usersController.create(userArray);
			}
		}
	).catch(
		error => result = error
	);
}

getUsers();

setInterval(function(){
	getUsers();
}, 3600000);

app.get('/', usersController.all);
// app.post('/users', usersController.create);