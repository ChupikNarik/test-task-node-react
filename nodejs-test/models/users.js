var db = require('../db');

exports.all = function(cb){
	db.get().collection('users').find().toArray(function(err, docs){
		cb(err, docs);
	})
}

exports.create = function (user, cb) {
	db.get().collection('users').insert(user, function (err, result) {
		cb(err, result);
	})
}

exports.clear = function(cb){
	db.get().collection('users').drop(function(err, result){
		cb(err, result);
	})
}