var Users = require('../models/users');

exports.all = function(req, res){
	Users.all(function(err, docs){
		if (err) {
			console.log(err);
			return res.sendStatus(500);
		}
		res.send(docs);
	})
}

exports.create = function (req, res) {
	var user = {
		login: req.login,
		name: req.name,
		location: req.location,
		email: req.email,
		bio: req.bio,
		url: req.url
	};
	Users.create(user, function (err, result) {
		if (err) {
			console.log(err);
			return res.sendStatus(500);
		}
	})
}

exports.clear = function(req, res){
	Users.clear(function(err, res){
		if (err) {
			console.log(err);
		}
		console.log('Done');
	});
}